# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ScinetItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    realm = scrapy.Field()
    realm_medium = scrapy.Field()
    realm_type = scrapy.Field()
    catid = scrapy.Field()
    name = scrapy.Field()
    uid = scrapy.Field()
    institute = scrapy.Field()
    title = scrapy.Field()
    visitors = scrapy.Field()
    articles = scrapy.Field()
    activity = scrapy.Field()
